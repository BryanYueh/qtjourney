#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QString>
#include <QLabel>
#include <QPainter>
#include <QMediaPlayer>
#include <QPushButton>
#include <QTime>
#include <QGridLayout>
#include <QTimer>
#include <QPalette>
#include <QPixmap>

class Window : public QWidget
{
    Q_OBJECT

public:
    Window(QWidget *parent = 0);
    ~Window();

public slots:
    void updateCurrentTime();
    void updateEspTime();
    void startEspTimer();

private:
    QPalette *m_pPalette;
    QPixmap *m_pPixmap;

    QTimer *timerCurrent;
    QTimer *timerEsp;

    QLabel *labelCurrent;
    QLabel *labelEsp;
    QPushButton *button;

    QTime TCurrent;
    QTime TEsp;

    QString strTCurrent;
    QString strTEsp;

    QMediaPlayer mediaPlayer;

    bool isStart;
};

#endif // WINDOW_H
