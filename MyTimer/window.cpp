#include "window.h"
#include <QDir>

#define DEBUG true

#ifdef DEBUG
#define BG_PATH "C:/Users/bryanyueh/Pictures/"
#define Vo_PATH "C:/Users/bryanyueh/Music/"
#else
#define BG_PATH "/home/bryanyueh/Public/Projects/qt/build-MyTimer-Desktop-Debug/Res/"
#define Vo_PATH "/home/bryanyueh/Public/Projects/qt/build-MyTimer-Desktop-Debug/Res/Voice_start.mp3"
#endif

Window::Window(QWidget *parent)
    : QWidget(parent)
{
    isStart = false;

    m_pPalette = new QPalette();
    QString strPath = BG_PATH;
    m_pPixmap = new QPixmap(strPath + "girl_00_00.jpg");
    m_pPalette->setBrush(QPalette::Background,QBrush(*m_pPixmap));
    setPalette(*m_pPalette);

    labelCurrent = new QLabel();
    TCurrent = QTime::currentTime();
    strTCurrent = TCurrent.toString(Qt::TextDate);
    labelCurrent->setText(strTCurrent);
    labelCurrent->setAlignment(Qt::AlignCenter);
    labelCurrent->setStyleSheet("color: red;"
                                "font: bold 140pt;");

    labelEsp = new QLabel();
    TEsp.setHMS(0,0,0,0);
    strTEsp = TEsp.toString(Qt::TextDate);
    labelEsp->setText(strTEsp);
    labelEsp->setAlignment(Qt::AlignCenter);
    labelEsp->setStyleSheet("color: orange;"
                            "font: bold 140pt;");

    button = new QPushButton();
    button->setText("START");
    button->setStyleSheet("font: bold 36pt;");
    connect(button, SIGNAL(pressed()), this, SLOT(startEspTimer()));

    QGridLayout *grid = new QGridLayout();

    grid->addWidget(labelCurrent,0,0);
    grid->addWidget(labelEsp,1,0);
    grid->addWidget(button,2,0);

    setLayout(grid);

    setWindowTitle(tr("My Timer"));
    resize(900, 600);

    timerCurrent = new QTimer(this);
    connect(timerCurrent, SIGNAL(timeout()), this, SLOT(updateCurrentTime()));
    timerCurrent->start(1000);

    timerEsp = new QTimer(this);
    connect(timerEsp, SIGNAL(timeout()), this, SLOT(updateEspTime()));
}

Window::~Window()
{

}


void Window::startEspTimer()
{
    if(!isStart){
        timerEsp->start(1000);
        TEsp.setHMS(0,0,0,0);
        strTEsp = TEsp.toString(Qt::TextDate);
        labelEsp->setText(strTEsp);

        QString strPath = Vo_PATH;
        mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice_start.mp3"));
        mediaPlayer.play();

        button->setText("STOP");
        isStart = true;
    }else{
        timerEsp->stop();
        button->setText("START");
        isStart = false;
    }
}

void Window::updateCurrentTime()
{
 TCurrent = QTime::currentTime();
 strTCurrent = TCurrent.toString(Qt::TextDate);
 labelCurrent->setText(strTCurrent);
}

void Window::updateEspTime()
{
    TEsp = TEsp.addSecs(1);
    strTEsp = TEsp.toString(Qt::TextDate);
    labelEsp->setText(strTEsp);    
    QString strPath = Vo_PATH;

    if(TEsp.minute() % 5 == 0 && TEsp.second() == 0){
        if(TEsp.hour() > 0){
            mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice_over30.mp3"));
            mediaPlayer.play();
        }else
        {
            switch (TEsp.minute() / 5)
            {
            case 1:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice5.mp3"));
                mediaPlayer.play();
                break;
            case 2:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice10.mp3"));
                mediaPlayer.play();
                break;
            case 3:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice15.mp3"));
                mediaPlayer.play();
                break;
            case 4:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice20.mp3"));
                mediaPlayer.play();
                break;
            case 5:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice25.mp3"));
                mediaPlayer.play();
                break;
            case 6:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice30.mp3"));
                mediaPlayer.play();
                break;
            default:
                mediaPlayer.setMedia(QUrl::fromLocalFile(strPath + "Voice_over30.mp3"));
                mediaPlayer.play();
                break;
            }
        }
    }
}
