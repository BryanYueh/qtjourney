#-------------------------------------------------
#
# Project created by QtCreator 2014-03-18T01:22:21
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyTimer
TEMPLATE = app


SOURCES += main.cpp\
        window.cpp

HEADERS  += window.h

