#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>

class QPushButton;
class Window : public QWidget
{
  Q_OBJECT
public:
  explicit Window(QWidget *parent = 0);
private:
  QPushButton* m_button;
signals:
private slots:
  void slotButtonClicked(bool isChecked);
};

#endif // WINDOW_H
