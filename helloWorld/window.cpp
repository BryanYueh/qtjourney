/*
 * It's a practice of signal and slot
 * action:
 * 1.click the button will launch multithread app.
*/
#include "window.h"
#include <QApplication>
#include <QPushButton>
#include <QProcess>

Window::Window(QWidget *parent) : QWidget(parent) {
  setFixedSize(300, 200);

  m_button = new QPushButton(this);
  m_button->setGeometry(100, 100, 100, 50);
  m_button->setText("Hello World!");
  m_button->setToolTip("It's a button, click to launch an app.");

  QFont font("Comic Sans MS");
  m_button->setFont(font);

  QIcon icon("C:/Users/bryanyueh/Downloads/good.png");
  m_button->setIcon(icon);

  //connect slotButtonClicked
  connect(m_button, SIGNAL(clicked(bool)), this, SLOT (slotButtonClicked(bool)));

}

void Window::slotButtonClicked(bool){
  QString strPath = "D:/QT/build-multithread-Desktop_Qt_5_6_0_MinGW_32bit-Release/release/multithread";
  QProcess *process = new QProcess(this);
  process->startDetached(strPath);
  m_button->setText("Launch the app");

}
