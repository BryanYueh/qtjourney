TEMPLATE = app
TARGET = name_of_the_app

QT += core
QT += widgets
QT += gui

SOURCES += \
    main.cpp \
    window.cpp

HEADERS += \
    window.h
