QT += core
QT -= gui

CONFIG += c++11

TARGET = Creating Threads
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    bthread.cpp \
    threadctr.cpp

HEADERS += \
    bthread.h \
    threadctr.h
