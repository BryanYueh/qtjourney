#ifndef BTHREAD_H
#define BTHREAD_H
#include <QThread>
#include <QString>
#include <QMutex>

class BThread : public QThread
{  
  Q_OBJECT
public:
    BThread(QString s, QMutex* m);
    void run();

private:
  QString name;
  QMutex* mutex;
};

#endif // BTHREAD_H
