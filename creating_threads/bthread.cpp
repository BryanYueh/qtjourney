#include "bthread.h"
#include <QDebug>

BThread::BThread(QString s, QMutex* m) : name(s), mutex(m) {

}

void BThread::run() {
    for(int i = 0; i < 10; ++i){
        mutex->lock();
        qDebug() << this->name << ": " << i;
        mutex->unlock();
    }
}
