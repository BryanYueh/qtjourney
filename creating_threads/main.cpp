#include <QCoreApplication>
#include "threadctr.h"

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);

  ThreadCtr tCtr;
  tCtr.Start();

  return a.exec();
}
