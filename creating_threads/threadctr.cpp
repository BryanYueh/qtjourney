#include "threadctr.h"
#include "bthread.h"
#include <QDebug>

ThreadCtr::ThreadCtr(QObject *parent) : QObject(parent)
{

}

void ThreadCtr::Start() {

  BThread *thread1 = new BThread("A", &mutex);
  BThread *thread2 = new BThread("B", &mutex);
  BThread *thread3 = new BThread("C", &mutex);

  connect( thread1, SIGNAL(finished()), thread1, SLOT(deleteLater()) );
  connect( thread2, SIGNAL(finished()), thread2, SLOT(deleteLater()) );
  connect( thread3, SIGNAL(finished()), thread3, SLOT(deleteLater()) );

  thread1->start();
  thread2->start();
  thread3->start();

}

