#ifndef THREADCTR_H
#define THREADCTR_H
#include <QObject>
#include <QMutex>

class ThreadCtr : public QObject
{
  Q_OBJECT
private:

public:
  explicit ThreadCtr(QObject *parent = 0);
  QMutex mutex;
  void Start();

signals:

public slots:
};

#endif // THREADCTR_H
