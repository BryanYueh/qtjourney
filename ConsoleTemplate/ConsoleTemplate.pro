#-------------------------------------------------
#
# Project created by QtCreator 2014-03-21T22:27:52
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Terminal
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    mainclass.cpp

HEADERS += \
    mainclass.h
