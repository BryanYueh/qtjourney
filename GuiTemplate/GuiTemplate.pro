#-------------------------------------------------
#
# Project created by QtCreator 2014-03-22T00:30:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GuiTemplate
TEMPLATE = app


SOURCES += main.cpp\
        mainclass.cpp

HEADERS  += mainclass.h
