#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QWidget>

class MainClass : public QWidget
{
    Q_OBJECT

public:
    explicit MainClass(QWidget *parent = 0);
    ~MainClass();
    void quit();

signals:
    /////////////////////////////////////////////////////////////
    /// Signal to finish, this is connected to Application Quit
    /////////////////////////////////////////////////////////////
    void finished();

public slots:
    /////////////////////////////////////////////////////////////
    /// slot that get signal when that application is about to quit
    /////////////////////////////////////////////////////////////
    void aboutToQuitApp();

private:
    QCoreApplication *app;

};

#endif // MAINCLASS_H
