#include "mainclass.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // create the main class
    MainClass myMain;

    // connect up the signals
    QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
    QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));

    myMain.show();

    return app.exec();
}
